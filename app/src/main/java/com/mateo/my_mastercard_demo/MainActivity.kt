package com.mateo.my_mastercard_demo

import android.content.Intent
import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import com.sygo.entidades.OperacionResult
import com.sygo.entidades.OperationInfo
import com.sygo.sygo.views.Sygo00
import kotlinx.android.synthetic.main.activity_main.*
import androidx.core.app.ComponentActivity.ExtraData
import androidx.core.content.ContextCompat.getSystemService
import android.icu.lang.UCharacter.GraphemeClusterBreak.T
import android.util.Log
import android.view.View
import kotlinx.android.synthetic.main.activity_main.view.*


class MainActivity : AppCompatActivity(){

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        btnPayment.setOnClickListener {
            sendData(CONSUMO, "1", null, SOLES, null, null, true)

        }
        btnAnnulment.setOnClickListener {
            sendData(ANULACION, "1", null, SOLES, null, "388", true)
        }
        btnReport.setOnClickListener {
            sendData(REPORTE_TOTAL,null,null,null,null,null, true)
        }

    }

    fun sendData(
        tipoOperacion: String, importe: String?, propina: String?,
        moneda: String?, codMozo: String?, codReferencia: String?, isPopUp: Boolean
    ) {
        val intent = Intent(this, Sygo00::class.java)
        val o = OperationInfo()
        o.tipoOperacion = tipoOperacion
        if (importe != null)
            o.importe = java.lang.Double.parseDouble(importe)
        if (propina != null)
            o.propina = java.lang.Double.parseDouble(propina)
        if (moneda != null)
            o.tipoMoneda = moneda
        if (codMozo != null)
            o.codMozo = codMozo
        if (codReferencia != null)
            o.codReferencia = codReferencia
        o.isPopUp = isPopUp
        val bundle = Bundle()
        bundle.putSerializable(OperationInfo.OPERATION_INFO, o)
        bundle.putString("entorno", "P")
        intent.putExtras(bundle)
        startActivityForResult(intent, REQUEST_CODE)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == REQUEST_CODE) {
            val builder = StringBuilder()
            if (data != null) {
                val bundle = data.extras
                if (bundle != null) {
                    val o = (bundle.get(OperacionResult.OPERATION_RESULT) as OperacionResult?)!!
                    if (o.typeResult == OperacionResult.TypeResult.ERROR) {
                        builder.append("ResponseCode = ").append(o.responseCode).append(ENTER)
                        builder.append("ResponseMessage = ").append(o.responseMessage).append(ENTER)
                    } else {
                        if (o.typeResult == OperacionResult.TypeResult.CONSUMO) {
                            builder.append("TransactionBuyerCard = ").append(o.card).append(ENTER)
                            builder.append("TransactionCardBrand = ").append(o.transactionCardBin)
                                .append(ENTER)
                            builder.append("TransactionCard = ").append(o.transactionCard).append(
                                ENTER)
                            builder.append("TransactionBuyerName = ").append(o.buyerName).append(
                                ENTER)
                            builder.append("TransactionResult = ").append(o.transactionResult)
                                .append(ENTER)
                            builder.append("TransactionCurrency = ").append(o.transactionCurrency)
                                .append(ENTER)
                            builder.append("TransactionAmount = ").append(o.transactionAmount)
                                .append(ENTER)
                            builder.append("TransactionReferenceNumber = ")
                                .append(o.transactionReferenceNumber).append(ENTER)
                            builder.append("TransactionCuotasNumber = ")
                                .append(o.transactionCuotasNumber).append(ENTER)
                            builder.append("TransactionId = ").append(o.transactionId).append(ENTER)
                            builder.append("TransactionDate = ").append(o.transactionDate)
                                .append(ENTER)
                            builder.append("TransactionTime = ").append(o.transactionTime)
                                .append(ENTER)
                            builder.append("ResponseCode = ").append(o.responseCode).append(ENTER)
                            builder.append("ResponseMessage = ").append(o.responseMessage)
                                .append(ENTER)

                        } else {
                            builder.append(o.voucher)
                        }
                    }
                    tvResult.setText(builder.toString())
                }
            } else {
                tvResult.setText("didn´t get a thing")
            }
        }
    }
}
