package com.mateo.my_mastercard_demo

const val REQUEST_CODE = 666
const val CONSUMO = "consumo"
const val REPORTE_TOTAL = "total"
const val REPORTE_DETALLADO = "detallado"
const val REPORTE_MOZO = "mozo"
const val REIMPRESION = "copia"
const val ANULACION = "anulacion"
const val SOLES = "PEN"
const val ENTER = "\r\n"